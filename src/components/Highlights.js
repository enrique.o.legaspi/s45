import React from 'react';
import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights() {
	return(
			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Learn from Home</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et amet reprehenderit iste laboriosam odit fugiat repellat unde cupiditate perferendis quam est blanditiis veritatis, iusto dolore, quos ipsa praesentium ex ipsum! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et amet reprehenderit iste laboriosam odit fugiat repellat unde cupiditate perferendis quam est blanditiis veritatis, iusto dolore, quos ipsa praesentium ex ipsum!
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et amet reprehenderit iste laboriosam odit fugiat repellat unde cupiditate perferendis quam est blanditiis veritatis, iusto dolore, quos ipsa praesentium ex ipsum! Lorem ipsum dolor sit amet, consectetur adipisicing elit.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Be part of our Community</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et amet reprehenderit iste laboriosam odit fugiat repellat unde cupiditate perferendis quam est blanditiis veritatis, iusto dolore, quos ipsa praesentium ex ipsum! Lorem ipsum dolor sit amet.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)

}