import React from 'react';
import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function ArchiveCourse({course, isActive, fetchData}) {

	const archiveToggle = (courseId) => {
		fetch(`http://localhost:4000/courses/${courseId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully archived'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Ooops! Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
				fetchData()
			}

		})
	}

	const reActivateToggle = (courseId) => {
		fetch(`http://localhost:4000/courses/${courseId}/reactivate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully reactivated'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Ooops! Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
				fetchData()
			}

		})
	}

	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(course)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => reActivateToggle(course)}>Unarchive</Button>

			}
		</>
		)
}