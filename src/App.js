import React, {useState} from 'react';
import './App.css';
import {Container, Row, Col} from 'react-bootstrap';
import ErrorPage from './components/ErrorPage';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Login from './pages/Login';
import PageNotFound from './pages/PageError'
import SpecificCourse from './pages/SpecificCourse';

import { UserProvider } from './UserContext';

// For routes
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={ <Home /> }/>
            <Route path="/courses" element={ <Courses /> }/>
            <Route path="/register" element={ <Register /> }/>
            <Route path="/login" element={ <Login /> }/>
            <Route path="/logout" element={ <Logout /> }/>
            <Route path="/courses/:courseId" element={<SpecificCourse />} />
            <Route path="*" element={ <ErrorPage /> }/>
          </Routes>
        </Container>
      </Router>  
    </UserProvider>
  );
}

export default App;
