import React, { useContext, useState, useEffect } from 'react';
// import coursesData from '../mockData/coursesData';
import CourseCard from '../components/CourseCard';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

import UserContext from '../UserContext';

export default function Courses() {
	
	const { user } = useContext(UserContext);

	const [allCourses, setAllCourses] = useState([]);

	const fetchData = () => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			
			setAllCourses(data);
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	return (
			// <>
			// 	<h1>Courses</h1>
			// 	{courses}
			// </>

			<>
				{(user.isAdmin === true) ?
					<AdminView coursesData={allCourses} fetchData={fetchData} />

					:

					<UserView coursesData={allCourses} />

				}
			</>
				

		)
}